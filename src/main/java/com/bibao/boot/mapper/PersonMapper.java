package com.bibao.boot.mapper;

import java.util.List;

import com.bibao.boot.model.Person;
import com.bibao.boot.model.PersonListResponse;
import com.bibao.boot.model.PersonResponse;

public class PersonMapper {
	public static PersonResponse mapToPersonResponse(Person person, int code, String message) {
		PersonResponse response = new PersonResponse();
		response.setPerson(person);
		response.setCode(code);
		response.setMessage(message);
		return response;
	}
	
	public static PersonResponse mapToPersonResponse(int code, String message) {
		PersonResponse response = new PersonResponse();
		response.setCode(code);
		response.setMessage(message);
		return response;
	}
	
	public static PersonListResponse mapToPersonListResponse(List<Person> personList, int code, String message) {
		PersonListResponse response = new PersonListResponse();
		response.setPersonList(personList);
		response.setCode(code);
		response.setMessage(message);
		return response;
	}
}
