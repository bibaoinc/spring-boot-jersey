package com.bibao.boot.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.bibao.boot.dao.PersonDao;
import com.bibao.boot.mapper.PersonMapper;
import com.bibao.boot.model.Person;

@Component
@Path("/person")
public class PersonRestService {
	@Autowired
	private PersonDao personDao;
	
	@GET
	@Path("/ping")
	@Produces(MediaType.TEXT_PLAIN)
	public Response ping() {
		return Response.status(200).entity("Ping Person Rest Service").build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response findAllPerson() {
		List<Person> personList = personDao.findAll();
		if (CollectionUtils.isEmpty(personList)) {
			String message = "No person is found";
			return Response.status(200).entity(PersonMapper.mapToPersonResponse(204, message)).build();
		}
		String message = "Successfully retrieved all person records";
		return Response.status(200).entity(PersonMapper.mapToPersonListResponse(personList, 200, message)).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response findPersonById(@PathParam("id") int id) {
		Person person = personDao.findById(id);
		if (person==null) {
			String message = "No record is found for id " + id;
			return Response.status(200).entity(PersonMapper.mapToPersonResponse(204, message)).build();
		}
		String message = "Successfully retrieved the employee with id " + id;
		return Response.status(200).entity(PersonMapper.mapToPersonResponse(person, 200, message)).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response savePerson(Person person) {
		try {
			Person savedPerson = personDao.save(person);
			String message = "Successfully saved the person";
			return Response.status(200).entity(PersonMapper.mapToPersonResponse(savedPerson, 200, message)).build();
		} catch (Exception e) {
			return Response.status(400).entity(PersonMapper.mapToPersonResponse(400, e.getMessage())).build();
		}
	}
	
	@PUT
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response updateEmp(Person person) {
		try {
			Person savedPerson = personDao.update(person);
			String message = "Successfully updated the person";
			return Response.status(200).entity(PersonMapper.mapToPersonResponse(savedPerson, 200, message)).build();
		} catch (Exception e) {
			return Response.status(400).entity(PersonMapper.mapToPersonResponse(400, e.getMessage())).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response deleteEmp(@PathParam("id") int id) {
		try {
			personDao.deleteById(id);
			String message = "Successfully deleted the person with id " + id;
			return Response.status(200).entity(PersonMapper.mapToPersonResponse(200, message)).build();
		} catch (Exception e) {
			return Response.status(400).entity(PersonMapper.mapToPersonResponse(200, e.getMessage())).build();
		}
	}
}
