package com.bibao.boot.springbootjersey;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.bibao.boot.rest.PersonRestService;

@Component
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		register(PersonRestService.class);
	}
}
