package com.bibao.boot.springbootjersey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.bibao.boot")
public class SpringBootJerseyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJerseyApplication.class, args);
	}
}
